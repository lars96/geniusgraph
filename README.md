# GeniusGraph #

## Do you want your own GeniusGraph? [Sign up here!](https://genius.com/discussions/273361-Get-your-own-geniusgraph) ##
## Möchtest du deinen eigenen GeniusGraph? [Registriere dich jetzt!](https://genius.com/discussions/272274-Geniusgraph-jetzt-fur-jeden) ##

# English

* What is GeniusGraph?
* Prerequisites
* Installation

### What is GeniusGraph? ###

GeniusGraph provides the user a way to connect to the Genius API and export the IQ trend to an image through a python implementation.
All exported images (as demo) can be found [here](https://larsbutnotleast.xyz/genius/). The `auth` directory does not belong to GeniusGraph; it's part of [GeniusBadge](https://bitbucket.org/lars96/geniusbadge).

### Prerequisites ###

To run GeniusGraph the recommended environment is a Debian based operating system with network connection. For proper usage the [Raspberry Pi](https://www.raspberrypi.org/) (no matter which model) is recommended.
Dependencies that need to be installed:

* Python 2.7 (at least)
* Python libraries (installable by using [pip package installer](https://pypi.python.org/pypi/pip)):
	* Matplotlib (Windows: py -m pip install Matplotlib)  (Linux: sudo pip install Matplotlib)
	* MySQL-python (Windows: py -m pip install MySQL-python)  (Linux: sudo pip install MySQL-python)
	* httplib2 (Windows: py -m pip install httplib2)  (Linux: sudo pip install httplib2)
	* numpy (Windows: py -m pip install numpy)  (Linux: sudo pip install numpy)
* MySQL server and database
* Optional: Apache2 webserver

### Installation ###

* First of all you will need to clone the repository
* The MySQL server needs to be set up and a database needs to be created (default name is **geniusdb**)
* Optional: user and password for the database can be created as well, otherwise **root** will also work
* You need to import init.sql to your database
* The SQL credentials need to be filled in into the variables in genius.py, see #CONFIG VARIABLES section
* Head over to [the Genius documentation](https://docs.genius.com/) and grab an Auth token, either by clicking **Authorize with Genius** or registering your own application
* Create the desired account(s) in the graph database. Sample INSERT statement, see sample.sql
* Call the python script using *python genius.py*. Make sure you are privileged enough to execute the script and write to the location.
* You're almost done! If you wish you can use the script with a [cronjob](https://cron-job.org/en/). I recommend checking either every hour or once a day.
	* every hour:            *`0 * * * * cd /home/pi/Desktop/genius/; /usr/bin/python /home/pi/Desktop/genius/genius.py`*
	* every day at midnight: *`0 0 * * * cd /home/pi/Desktop/genius/; /usr/bin/python /home/pi/Desktop/genius/genius.py`*

# German

* Was ist GeniusGraph?
* Vorbereitungen
* Installation

### Was ist GeniusGraph? ###

GeniusGraph erlaubt es Nutzern, ihren Account zur Genius-API zu verbinden und ihre IQ-Entwicklung durch ein vom Programm generiertes Bilddiagramm zu verfolgen.
Alle exportierten Bilder der Demo können [hier](https://larsbutnotleast.xyz/genius/) gefunden werden. Das `auth`-Verzeichnis gehört nicht zu GeniusGraph, es ist Teil der [GeniusBadge](https://bitbucket.org/lars96/geniusbadge).

### Prerequisites ###

Damit GeniusGraph fehlerfrei funktionieren kann wird ein auf Debian basierendes Betriebssystem mit Internetzugang empfohlen. Für das beste Ergebnis wird empfohlen, GeniusGraph auf einem [Raspberry Pi](https://www.raspberrypi.org/) (egal welches Modell) zu installieren.

Folgende Dependencies müssen installiert werden, damit GeniusGraph richtig läuft:

* (Mindestens) Python 2.7 
* Python Bibliotheken (können mit dem [PIP-Paketmanager](https://pypi.python.org/pypi/pip) installiert werden):
	* Matplotlib (Windows: py -m pip install Matplotlib)  (Linux: sudo pip install Matplotlib)
	* MySQL-python (Windows: py -m pip install MySQL-python)  (Linux: sudo pip install MySQL-python)
	* httplib2 (Windows: py -m pip install httplib2)  (Linux: sudo pip install httplib2)
	* numpy (Windows: py -m pip install numpy)  (Linux: sudo pip install numpy)
* MySQL-Server und Datenbank
* Optional: Apache2-Webserver

### Installation ###

* Zuerst musst du dieses Repositorium klonen.
* Der MySQL-Server muss eingestellt und einen Datenbank erstellt werden. (der standartmäßig eingestellte Name ist **geniusdb**)
* Optional: Nutzer und Passwort können ebenfalls für die Datenbank erstellt werden, andernfalls kann auch einfach **root** als benutzt werden.
* Du musst die Datei init.sql aus diesem Repositorium in deine Datenbank importieren.
* Die SQL-Anmeldedaten müssen in die Variablen in der genius.py-Datei aus diesem Repositorium eingefügt werden. (#CONFIG VARIABLES)
* Navigiere deinen Standardwebbrowser [zur Genius-API-Dokumentation](https://docs.genius.com/) und hole dir einen Auth-Token. Entweder indem du auf **Authorize with Genius** klickst oder indem du deine eigene App registrierst.
* Erstelle die gewünschten Account(s) in der Graph-Datenbank. Ein Beispielseintrag hierfür kann in der sample.sql-Datei in diesem Repositorium gefunden werden.
* Rufe das Python-Script auf, indem du *python genius.py* in dein Terminal eingibst. Vergewissere dich, dass deine zugewiesenen Systemrechte ausreichen, um das Script auszuführen und an diesem Ort Schreibrechte hast. 
* Du bist fast fertig! Wenn du willst, kannst du das Script mit [Cron-job](https://cron-job.org/en/) laufen lassen. Ich persönlich empfehle, jede Stunde oder zumindest jeden Tag das Script durchlaufen zu lassen.
	* jede Stunde:            *`0 * * * * cd /home/pi/Desktop/genius/; /usr/bin/python /home/pi/Desktop/genius/genius.py`*
	* jeden Tag um Mitternacht: *`0 0 * * * cd /home/pi/Desktop/genius/; /usr/bin/python /home/pi/Desktop/genius/genius.py`*