import httplib, urllib, json, MySQLdb, time, ConfigParser, easylogger, os.path, argparse, sys, matplotlib.ticker, matplotlib.dates
from easylogger import log, info, debug, DEBUG
import numpy as np
import matplotlib
### Force matplotlib to not use any Xwindows backend/use()-method needs to be called before any backend is chosen
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
from matplotlib.ticker import FormatStrFormatter
### pylab needs to be imported after matplotlib
import pylab
from datetime import datetime, timedelta

#global result, fetched_iq, iq_for_display, fetch, fetchdatetime_wo_seconds
fetch = 0 				#global count var, number of tuples
fetchdatetime = [] 		#array consisting of datetimes
fetchdatetime_wo_seconds = []
totalIQ = [] 			#array consisting of IQ values without decimal dots
displaytotalIQ = [] 	#totalIQ but with decimal dots
displaytotalIQ_wo_seconds = []
result = None
fetched_iq = 0
iq_for_display = 0.0
res_ = 0
user = ''
display_user = ''
#CONFIG VARIABLES - Fill in your credentials here
auth_token = ''
host = ''
db = ''
db_user = ''
db_pass = ''
#END CONFIG VARIABLES
export_path = ''
export_name = ''
user_ID = ''
Genius_user_ID = ''
program_mode = 0
program_mode_param = 0



### this method is used to establish a connection to api.genius.com to retrieve data from their API. It will get /account and store the current values in global vars
def getData():
        global fetched_iq, iq_for_display, user, display_user, Genius_user_ID

	conn = httplib.HTTPSConnection("api.genius.com")
	request_uri = "/account"
	headersMap = {
		"User-Agent": "CompuServe Classic/1.22",
		"Accept": "application/json",
		"Authorization": "Bearer " + auth_token
	}
	conn.request("GET", request_uri, headers=headersMap)
	response = conn.getresponse()

	log.debug('API response: {0}: {1}'.format(response.status, response.reason))

	if (response.status != 200):
		return False
	data = response.read()
	result = json.loads(data)
	
	fetched_iq = result["response"]["user"]["iq"];
	iq_for_display = result["response"]["user"]["iq_for_display"];
	user = result["response"]["user"]["login"]
	Genius_user_ID = result["response"]["user"]["id"]

	# for some reason, the username contains the \u200b character that needs to be removed for proper plotting later
	uni_name = result["response"]["user"]["name"]
    display_user = ''.join([i if ord(i) < 128 else '' for i in uni_name])

log.info('API reported a total of {0} IQ for user {1}!'.format(iq_for_display, user))
	return True

### this method will get any records that already exist in the db and compares the latest value against the fetched one. If there is a difference (higher or lower IQ), the new value will be inserted into the db
def insertData():
	global host, db, db_user, db_pass, export_path, latest_name, user, Genius_user_ID, user_ID
	con = MySQLdb.connect(host, db_user, db_pass, db)

	datetimevar = time.strftime('%Y-%m-%d %H:%M:%S');

	with con:
		cur = con.cursor()
		cur.execute("SELECT totalIQ FROM iqtrend WHERE username = '{0}' ORDER BY fetchdatetime DESC LIMIT 1".format(user))

		res = cur.fetchone()
		if cur.rowcount > 0:
			res_ = res[0];
			log.debug('Last IQ found in database was {0}'.format(res[0]))
		else:
			res_ = 0;

# insert the username and id into the graph table
		cur.execute("UPDATE graph SET username = '{0}', id = '{1}' WHERE userid = '{2}'".format(user, Genius_user_ID, user_ID))
		con.commit()

	diff = abs(res_ - fetched_iq)
	log.debug('Old IQ value: {0}, new value: {1}. Difference is: {2}'.format(res_, fetched_iq, diff))

	if (res_ != fetched_iq):
		log.info('IQ value changed compared to the last check. Beginning insertion into database')		
		cur.execute("INSERT INTO iqtrend(username, fetchdatetime, totalIQ, displaytotalIQ) VALUES('{0}', '{1}', '{2}', '{3}')".format(user, datetimevar, fetched_iq, iq_for_display))
		con.commit()

		cur.execute("UPDATE iqtrend SET username = '{0}' WHERE username = '{1}'".format(Genius_user_ID, user))
		con.commit()

		getDataForPlotting()
		plotData()
	else:
		log.info('Nothing to do here, IQ value unchanged')

def stringStatement():
	global user, program_mode_param, Genius_user_ID
	stmt = ""
	stmt = "SELECT fetchdatetime, totalIQ, displaytotalIQ FROM iqtrend WHERE username = '{0}' ORDER BY fetchdatetime DESC LIMIT {1}".format(Genius_user_ID, "9999") #str(program_mode_param))
	return stmt

def checkExportDir():
	if not os.path.exists(export_path):
		os.makedirs(export_path)
		log.info('Export directory did not exist. Created at {0}'.format(export_path))

def getDataForPlotting():
	global host, db, db_user, db_pass, fetch
	con = MySQLdb.connect(host, db_user, db_pass, db);
	
	with con:
		cur = con.cursor()
		str = stringStatement()
		cur.execute(str)

		log.debug('Found {0} rows with statement: {1}'.format(cur.rowcount, str))
		res = cur.fetchall()
	for i in reversed(res):
		fetch += 1
		fetchdatetime.append(i[0])
		fetchdatetime_wo_seconds.append(fetch)
		totalIQ.append(i[1])
		displaytotalIQ.append(i[2])

def plotData():
	fig = plt.figure()
	fig.add_subplot(111)
	date_range = 'IQ (' + fetchdatetime[0].date().strftime("%B %d, %Y") + ' - ' + fetchdatetime[len(fetchdatetime) - 1].date().strftime("%B %d, %Y") + ')'

	str_changes = 'change' if fetch == 1 else 'changes'
	xfig, ax = plt.subplots()
	ax.set_ylabel("IQ")
	ax.set_title('IQ development for {0} ({1} {2} on record)'.format(display_user, fetch, str_changes))
	ax.get_yaxis().set_major_formatter(matplotlib.ticker.FuncFormatter(lambda x, p: format(int(x), ',')))
	plt.xticks(rotation=70)
	plt.grid = True
	datestr = time.strftime('%Y%m%d_%H%M%S');
	plt.autolayout = True

	plt.gcf().subplots_adjust(bottom=0.25)
        plt.plot(fetchdatetime, totalIQ, label='' + date_range)
        plt.legend(loc='best')
	ax.grid(True)
	checkExportDir()
	plt.savefig(export_path + '/' + datestr + '.jpg')
	plt.savefig(export_path + '/' + latest_name + '.jpg')
	plt.close()

def parseConfig(auth, uid):
    global auth_token, export_path, latest_name, user_ID, fetch, fetchdatetime, fetchdatetime_wo_seconds, totalIQ, displaytotalIQ
	fetch = 0
	fetchdatetime[:] = []
	fetchdatetime_wo_seconds[:] = []
	totalIQ[:] = []
	displaytotalIQ[:] = []
	auth_token = auth
	export_path = '/var/www/html/geniusgraph/users/'
	latest_name = 'latest'
	user_ID = uid
	export_path = os.path.join(export_path, user_ID)
	log.info('...............................')
	log.info('Running for user ID: {0}. Plots will be saved at {1}'.format(user_ID, export_path))

log.info('...............................')
log.info('Starting GeniusGraph at ' + time.strftime('%Y-%m-%d %H:%M:%S'))

#Go for it...
conn = MySQLdb.connect(host, db_user, db_pass, db);
with conn:
	cur = conn.cursor()
	str = "SELECT token, userid FROM graph WHERE active = 1 and email_ai = 1"
	cur.execute(str)
	log.debug('Found {0} active accounts'.format(cur.rowcount))
 	res = cur.fetchall()

	for i in res:
		parseConfig(i[0], i[1])
		if getData():
			insertData()
		else:
			log.critical("AuthToken for user ID {0} returned invalid status!".format(user_ID))
		time.sleep(300)
log.info('Finishing......................')
