import httplib, urllib, json, MySQLdb, time, datetime, ConfigParser
import numpy as np
import matplotlib
### Force matplotlib to not use any Xwindows backend/use()-method needs to be called before any backend is chosen
matplotlib.use('Agg')
import matplotlib.pyplot as plt
### pylab needs to be imported after matplotlib
import pylab

global result, fetched_iq, iq_for_display, fetch, fetchdatetime_wo_seconds
fetch = 0 			#global count var, number of tuples
fetchdatetime = [] 		#array consisting of datetimes
fetchdatetime_wo_seconds = []
totalIQ = [] 			#array consisting of IQ values without decimal dots
displaytotalIQ = [] 		#totalIQ but with decimal dots
displaytotalIQ_wo_seconds = []
result = None
fetched_iq = 0

iq_for_display = 0.0

### this method is used to establish a connection to api.genius.com to retrieve data from their API. It will get /account and store the current values of iq and iq_for_display in global vars
### IMPORTANT: you will need to provide a valid auth_token to the method. The token can be created at https://docs.genius.com/ (Authorize with Genius).
def getData(auth_string):
    conn = httplib.HTTPConnection("api.genius.com")
    request_uri = "/account"
    headersMap = {
            "User-Agent": "CompuServe Classic/1.22",
            "Accept": "application/json",
            "Authorization": "Bearer " + auth_string
    }
    conn.request("GET", request_uri, headers=headersMap)
    response = conn.getresponse()
    ### Output the HTTP status code and reason text...
    #print response.status, response.reason
    data = response.read()

    result = json.loads(data)
    global fetched_iq, iq_for_display
    fetched_iq = result["response"]["user"]["iq"];
    iq_for_display = result["response"]["user"]["iq_for_display"];

    ###fields that can be retrieved:
    #iq = result["response"]["user"]["iq"]
    #followed_users_count = result["response"]["user"]["followed_users_count"]
    #followers_count = result["response"]["user"]["followers_count"]
    #role_for_display = result["response"]["user"]["role_for_display"]
    #annotations_count = result["response"]["user"]["stats"]["annotations_count"]
    #id = result["response"]["user"]["id"]
    #login = result["response"]["user"]["login"]

### this method will get any records that already exist in the db and compares the latest value against the fetched one. If there is a difference (higher or lower IQ), the new value will be inserted into the db
def insertData(host, db, db_user, db_pass, path, name):
    con = MySQLdb.connect(host, db_user, db_pass, db);

    #feel free to use your username ;) 
    username = "homesweethole";

    datetimevar = time.strftime('%Y-%m-%d %H:%M:%S');

    with con:
        cur = con.cursor()
        cur.execute("SELECT totalIQ FROM iqtrend WHERE username = '{0}' ORDER BY fetchdatetime DESC LIMIT 1".format(user))

        res = cur.fetchone()
        if cur.rowcount > 0:
            res_ = res[0];
        else:
            res_ = 0;
        
        print res, fetched_iq            
        #if table is empty or IQ has in/decreased since last check => insert into db
        if (res_ != fetched_iq):
            print 'Debug output: IQ changed since last check: Last = ' + str(res_) + '; Current = ' + str(fetched_iq)
            cur.execute("INSERT INTO iqtrend(username, fetchdatetime, totalIQ, displaytotalIQ) VALUES('homesweethole', '{0}', '{1}', '{2}')".format(datetimevar, fetched_iq, iq_for_display));
            getDataForPlotting(host, db, db_user, db_pass)
            plotData(path, name)
        else:
            print "Debug output: IQ not changed since last check! Exiting..."

def getDataForPlotting(host, db, db_user, db_pass):
    con = MySQLdb.connect(host, db_user, db_pass, db);
    datetimevar = time.strftime('%Y-%m-%d %H:%M:%S');
    
    with con:
        cur = con.cursor()
        str = "SELECT fetchdatetime, totalIQ, displaytotalIQ FROM iqtrend WHERE username = '{0}' ORDER BY fetchdatetime DESC LIMIT 15".format(user)
        cur.execute(str)

        res = cur.fetchall()
        global fetch
        for i in reversed(res):
            fetch += 1
            fetchdatetime.append(i[0])
            fetchdatetime_wo_seconds.append(fetch)
            totalIQ.append(i[1])
            displaytotalIQ.append(i[2])
 
### uncomment for debugging purposes and print every datetime + IQ       
#        for i in range(0, fetch):
#            print fetchdatetime[i]
#            print totalIQ[i]
#            print displaytotalIQ[i]
#            print "___________"

def plotData(path, name):
### old version using native pylab
#    x = pylab.linspace(0, 1)
#    pylab.plot(fetchdatetime, totalIQ, label="IQ")
#    
#    #pylab.xticks(fetchdatetime, fetchdatetime_wo_seconds)
#    pylab.xticks(fetchdatetime, fetchdatetime)
#    pylab.xticks(rotation=70)
#    pylab.yticks(totalIQ, displaytotalIQ)
#    pylab.yticks(rotation=30,size=7)
#    pylab.legend(loc='upper left')
#    pylab.title("Last " + str(fetch) + " IQ changes for user " + user )
#    pylab.autolayout = True
#    #pylab.autofmt_xdate()
#    #fig = pyplot.figure()
#    #fig.autofmt_xdate()
#    pylab.tight_layout()
#    #pylab.savefig('foo456.png')
#
#    datestr = time.strftime('%Y%m%d_ %H%M%S');
#    pylab.savefig('/var/www/genius/' + datestr + '.jpg')
#    pylab.savefig('/var/www/genius/latest.jpg')

### new version using pyplot
     fig = plt.figure()
     fig.add_subplot(111)
     date_range = 'IQ (' + str(fetchdatetime[0].date().strftime("%B %d")) + ' - ' +  str(fetchdatetime[len(fetchdatetime) - 1].date().strftime("%B %d")) + ')'
     
     plt.plot(fetchdatetime, totalIQ, label=date_range)
     plt.title("Last " + str(fetch) + " IQ changes for user " + user + "\n")
     plt.autolayout = True
     plt.xticks(rotation=70)
     plt.tight_layout()
     plt.legend(loc='upper left')
     plt.grid = True
     datestr = time.strftime('%Y%m%d_%H%M%S');
     plt.savefig(path + '/' + datestr + '.jpg')
     plt.savefig(path + '/' + name + '.jpg')

### Creating an instance of ConfigParser for safely using Bitbucket with the original script ;)
Config = ConfigParser.ConfigParser()
Config.read('genius.cfg')

auth_token = Config.get('Auth', 'Auth')
user       = Config.get('Auth', 'User')

host    = Config.get('Database', 'Host')
db      = Config.get('Database', 'Database')
db_user = Config.get('Database', 'User')
db_pass = Config.get('Database', 'Password')

export_path = Config.get('Path', 'Location')
latest_name = Config.get('Path', 'LatestName')

getData(auth_token)
insertData(host, db, db_user, db_pass, export_path, latest_name)
### getDataforPlotting() and plotData() will be called in insertData(). Uncomment for directly generating an image out of data from the db. Comment out the two methods above to reduce API traffic
#getDataForPlotting(host, db, db_user, db_pass)
#plotData(export_path, latest_name)
