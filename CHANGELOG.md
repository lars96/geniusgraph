##GeniusGraph Changelog:##

#April  4 2017#

* Updated to Version 2
* Excluded genius.cfg
* Moved old content to /v1
* Included the graph table to manage user accounts

#Feb   21 2017#
* Included links to use the GeniusGraph service

#Jan    2 2017#
* Corrected the cronjob syntax

#Dec   23 2016#
* Initial commit