CREATE TABLE IF NOT EXISTS `iqtrend` (
 `username` varchar(100) NOT NULL,
 `fetchdatetime` datetime NOT NULL,
 `totalIQ` int(11) NOT NULL,
 `displaytotalIQ` varchar(10) NOT NULL,
 PRIMARY KEY (`username`,`fetchdatetime`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1

CREATE TABLE IF NOT EXISTS `graph` (
 `email` varchar(100) NOT NULL,
 `email_ai` int(11) NOT NULL,
 `token` varchar(100) NOT NULL,
 `userid` varchar(30) NOT NULL,
 `username` varchar(100) DEFAULT '???',
 `active` int(1) NOT NULL DEFAULT '0',
 `registered` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=112 DEFAULT CHARSET=latin1;